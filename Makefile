SRC = 01_nfa 02_algo 03_regexp 04_logics 05_wmso 06_presburger 07_starfree 08_afa 09_games 10_fta 11_omega 12_buchi 13_complementation 14_muller 15_latest_app_rec 16_termination 17_ltl_aba
LIB = asl.sty
OBJ = $(SRC:=.pdf)

all : $(OBJ) $(LIB)
	make clean

%.pdf : %.tex $(LIB)
	pdflatex $<


.PHONY : clean veryclean

clean : 
	rm -f *.aux *.out *.log *.nav *.snm *.vrb *.toc

veryclean :
	rm -f *.aux *.out *.log *.nav *.snm *.vrb *.toc *~ *.pdf
